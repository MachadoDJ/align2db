#-*- coding: utf-8 -*-

# align2db.py

# v20150813

"""
Copyright (C) 2015 - Denis Jacob Machado
GNU General Public License version 3.0
Email: denisjacobmachado@gmail.com
Homepage: https://about.me/machadodj
***
Requires Biopython libraries and the NCBI BLAST+ suite
(available at
http://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=
BlastDocs&DOC_TYPE=Download)
***
You can create local databases with the makeblastdb program
from the NCBI BLAST+ suite
"""

## ARGS & LIBS #############################################

try:
    from multiprocessing import Process,Queue
except:
    sys.stderr.write(">ERROR: Could not import Process and Queue from multiprocessing\n")
    sys.stderr.flush()
    exit()
try:
    from Bio.Blast.Applications import NcbiblastnCommandline
except:
    sys.stderr.write(">ERROR: Could not import NcbiblastxCommandline from Bio.Blast.Applications\n")
    sys.stderr.flush()
    exit()
try:
    from Bio.Blast import NCBIXML
except:
    sys.stderr.write(">ERROR: Could not import NCBIXML from Bio.Blast\n")
    sys.stderr.flush()
    exit()
try:
    from Bio import SeqIO
except:
    sys.stderr.write(">ERROR: Could not import SeqIO from Bio\n")
    sys.stderr.flush()
    exit()
try:
    import argparse,os,sys
except:
    sys.stderr.write(">ERROR: importing argparse, os and/or sys\n")
    sys.stderr.flush()
    exit()
parser=argparse.ArgumentParser()
parser.add_argument("-q","--query",help="query sequence",type=str)
parser.add_argument("-d","--database",help="local blast database",type=str)
parser.add_argument("-f","--format",help="fasta or fastq (default)",type=str,default="fastq")
parser.add_argument("-o","--output",help="output prefix (default = alignments)",type=str,default="alignments")
parser.add_argument("-e","--evalue",help="e-value cutoff (default = 1e-5)",type=str,default="1e-5")
parser.add_argument("-p","--processes",help="number of processes to run simultaneously (default = 4)",type=int,default=4)
parser.add_argument("-z","--gzip",help="use for compressed files (.gz)",action="store_true",default=False)
parser.add_argument("-k","--keep_xml",help="keep alignments in XML format (default = false)",action="store_true",default=False)
parser.add_argument("-v","--verbose",help="increase output verbosity",action="store_true",default=False)
parser.add_argument("-V","--version",help="print version number and close",action="store_true",default=False)
args=parser.parse_args()

if(args.version):
    sys.stderr.write("version 2015.08.13\n")
    sys.stderr.flush()
    exit()

if(args.gzip):
    try:
        import gzip
    except:
        sys.stderr.write(">ERROR: could not import gzip\n")
        sys.stderr.flush()
        exit()

## FUNCTIONS ###############################################

def main():
    if(args.verbose):
        sys.stderr.write("> Reading %s (format = %s)\n# ID : E-VALUE\n"%(args.query,args.format))
        sys.stderr.flush()
    if(args.gzip):
        input_seqs=gzip.open(args.query,"rb")
    else:
        input_seqs=open(args.query,"rU")
    count_recs=0
    count_matches=0
    all_records=SeqIO.parse(input_seqs,args.format)
    threads=True
    while threads==True:
        results={} # results dictionary
        jobs=[] # current job list
        queue=Queue() # results queue
        for i in range(args.processes):
             # create and start jobs
            try:
                record=all_records.next()
            except:
                threads=False
                break
            else:
                count_recs+=1
                p=Process(target=workit,args=(record,count_recs,queue,))
                jobs.append(p)
                p.start()
        for p in jobs:
            p.join() # wait processes to finish
        for i in range(len(jobs)):
            results.update(queue.get()) # get results from queue
        for key in results:
            sys.stdout.write("%s | %s\n"%(key,results[key]))
            sys.stdout.flush()
    input_seqs.close()

def workit(record,count_recs,queue):
    resultdic={}
    xmlfilename="%s_%d.xml"%(args.output,count_recs)
    blastn_cline=NcbiblastnCommandline(task="blastn",db=args.database,outfmt=5,evalue="%s"%(args.evalue),max_target_seqs=1,out=xmlfilename,dust="no")
    blastn_cline(stdin=">%s\n%s"%(record.id,record.seq))
    xmlresult=open(xmlfilename,"rU")
    blast_records=NCBIXML.parse(xmlresult)
    item=next(blast_records)
    for alignment in item.alignments:
        for hsp in alignment.hsps:
            resultdic={record.id:hsp.expect}
            break
    xmlresult.close()
    if(not args.keep_xml):
        os.remove(xmlfilename)
    queue.put(resultdic)

main()

exit()