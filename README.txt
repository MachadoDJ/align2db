align2db and extractAligned v20150813 - Copyright (C) 2015 - Denis Jacob Machado

###########
# LICENSE #
###########

redux - Copyright (C) 2015 - Denis Jacob Machado

The align2db and extractAligned programs have ABSOLUTELY NO WARRANTY!

Programs are available at www.ib.usp.br/grant/anfibios/researchSoftware.html.

The program may be freely used, modified, and shared under the GNU General Public License
version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).
See LICENSE.txt for more details.

################
# AVAILABILITY #
################

* Official URL: http://www.ib.usp.br/grant/anfibios/researchSoftware.html
* GitLab: https://gitlab.com/MachadoDJ/align2db
* Online manual: https://gitlab.com/MachadoDJ/align2db/wikis/home
* Author's e-mail: denisjacobmachado[at]gmail.com

###########
# VERSION #
###########

version 20150813

###############
# DESCRIPTION #
###############

A parallelized tool to align large sequence files against a local database. This project
was originally designed to align mitochondrial reads from total genomic libraries against
a mitochondrial genome database.

#############
# CRITERIAS #
#############

Sequences will be aligned against a local database (using the program BlastN and a giver
e-value cut-off). The user may decide to select all aligned reads (criteria "1") or only
the reads in which both pairs have been aligned to the local database (criteria "0", used
by default). The selection criteria may be changed using command argument "-c" or
"--criteria" in extractAligned.

#########
# INPUT #
#########

The program align2db takes one sequence file in FASTA or FASTQ format. Files may be
compressed with GZIP.

The program extractAligned takes one or two sequence files in FASTA or FASTQ format. Files
may be compressed with GZIP. Criteria "0" requires two input files. The program
extractAligned also takes one or two text files with sequence ids and e-values (as printed
by align2db into STDOUT).

##########
# OUTPUT #
##########

The program align2db will print all aligned reads to Python's standart error (STDOUT).

The program extractAligned will print all selected reads directly into sequence files. If
the option "-p" or "--pruned" is used, un-selected sequences will also be saved into
different files.

NOTE: If the verbose option is on, output verbosity will increase considerably (most
likely resulting in additional time costs).

#############
# ARGUMENTS #
#############

(1) Argument options for align2db:
    -q, --query: query sequence
    -d, --database: the name of the local Blast database
    -f, --format: the input sequence format (FASTA or FASTQ; default = fastq)
    -o, --output: the prefix for the XML files produced by BlastN (default = alignments)
    -e, --evalue: the e-value cutoff (default = 1e-5)
    -p, --processes: number of processes to run simultaneously (default = 4)
    -z, --gzip: use for compressed GZIP files (default = off)
    -k, --keep_xml: keep alignments in XML format (default = off)
    -v, --verbose: increase output verbosity (default = off)
    -V, --version: print version number and quit (default = off)

(2) Argument options for extractAligned:
    -q, --queries: the names of the sequence files
    -a, --alignments: the names of the files with the alignements (from the STDOUT of align2db)
    -f, --format: the input sequence format (FASTA or FASTQ; default = fastq)
    -o, --output: the prefix for the output files (default = filtered)
    -e, --evalue: e-value cutoff (default = 1e-5)
    -c, --criteria: 0 [both pairs aligned to the databse] or 1 [any sequence that aligned to the database] (default = 0)
    -p, --pruned: saves sequence that were not selected into separate files
    -z, --gzi: use for compressed GZIP files (default = off)
    -v, --verbose: increase output verbosity (default = off)
    -V, --version: print version number and quit (default = off)

################
# DEPENDENCIES #
################

Requires Python 2.7.X or newer versions.

The following modules/ libraries are required:
* SeqIO from Bio
* argparse, os and sys


##################
# QUICK TUTORIAL #
##################

Imagine an experiment which generates reads in FASTQ format for total genomic reads (i.e.
nuclear DNA, mitochondrial DNA and maybe other sequences as well). Also imagine the
experiment results in paired-end reads that are saved in compressed GZIP files names
pair1.fastq.gz and pair2.fastq.gz.

Necessary files:
* align2db.py
* extractAligned.py
* pair1.fastq.gz
* pair2.fastq.gz
* Your local_database

TIP: You can create local databases with the makeblastdb program from the NCBI BLAST+
suite.

Steps:
1. Copy all the necessary files into to the working directory.

2. Execute align2db with following command lines:

    $ python align2db.py -z -q pair1.fastq.gz -f fastq -d local_database -e 1e-5 -p 4 >
    ids1.txt 2> align1.err

    $ python align2db.py -z -q pair2.fastq.gz -f fastq -d local_database -e 1e-5 -p 4 >
    ids2.txt 2> align2.err

The argument option "-p" or "--processes" defines the number of Python's subprocesses to
be run simultaneously. The sequence ID of the aligned reads will be saved into ids1.txt
and ids2.txt.

3. Execute extractAligned with the following command line:

    $ python extractAligned.py -z -q pair1.fastq.gz pair2.fastq.gz -f fastq -a ids1.txt
    ids2.txt -c 0 -o results -p > extract.out 2> extract.err

The argument option "-p" or "--pruned" makes the un-selected sequences be saved into
separated files with the output prefix and the "pruned" flag.
